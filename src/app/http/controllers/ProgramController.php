<?php

namespace Yeltrik\PdPSR\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Yeltrik\PdPSR\app\models\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Program::class);

        $programs = Program::all();
    }

    /**
     * @param Program $program
     * @throws AuthorizationException
     */
    public function show(Program $program)
    {
        $this->authorize('view', $program);

        $programTags = $program->tags;
    }

}
