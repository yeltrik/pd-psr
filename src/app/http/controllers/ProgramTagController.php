<?php

namespace Yeltrik\PdPSR\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\PdPSR\app\models\ProgramTag;
use Illuminate\Http\Request;

class ProgramTagController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', ProgramTag::class);

        $programTags = ProgramTag::all();
    }

    /**
     * @param ProgramTag $programTag
     * @throws AuthorizationException
     */
    public function show(ProgramTag $programTag)
    {
        $this->authorize('view', $programTag);

        $program = $programTag->program;
    }

}
