<?php

namespace Yeltrik\PdPSR\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\PdPSR\app\models\Roster;
use Illuminate\Http\Request;

class RosterController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Roster::class);

        $rosters = Roster::all();
    }

    /**
     * @param Roster $roster
     * @throws AuthorizationException
     */
    public function show(Roster $roster)
    {
        $this->authorize('view', $roster);

        $profile = $roster->profile;
        $rosterTags = $roster->tags;
    }

}
