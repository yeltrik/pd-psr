<?php

namespace Yeltrik\PdPSR\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\PdPSR\app\models\RosterTag;
use Illuminate\Http\Request;

class RosterTagController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', RosterTag::class);

        $rosterTags = RosterTag::all();
    }

    /**
     * @param RosterTag $rosterTag
     * @throws AuthorizationException
     */
    public function show(RosterTag $rosterTag)
    {
        $this->authorize('view', $rosterTag);

        $roster = $rosterTag->roster;
    }

}
