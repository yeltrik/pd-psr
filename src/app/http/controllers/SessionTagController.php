<?php

namespace Yeltrik\PdPSR\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\PdPSR\app\models\SessionTag;
use Illuminate\Http\Request;

class SessionTagController extends Controller
{

    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', SessionTag::class);

        $sessionTags = SessionTag::all();
    }

    /**
     * @param SessionTag $sessionTag
     * @throws AuthorizationException
     */
    public function show(SessionTag $sessionTag)
    {
        $this->authorize('view', $sessionTag);

        $session = $sessionTag->session;
    }

}
