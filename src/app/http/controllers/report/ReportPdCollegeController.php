<?php

namespace Yeltrik\PdPSR\app\http\controllers\report;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Yeltrik\PdPSR\app\report\Attendance;
use Yeltrik\UniMbr\app\models\Rank;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniTrm\app\models\Term;

class ReportPdCollegeController extends Controller
{

    /**
     * ReportPdCollegeController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $colleges = College::all();
        return view('pdPSR::report.pd.college.index', compact('colleges'));
    }

    /**
     * @param College $college
     * @return Application|Factory|View
     */
    public function show(College $college)
    {
        $terms = Term::all();

        $uniqueAttendanceByDepartment = [];
        foreach ( $college->departments as $department ) {
            $attendance = new Attendance();
            $attendance->forDepartment($department);
            $attendance->unique(TRUE);
            $query = $attendance->buildQuery();
            $uniqueAttendanceByDepartment[$department->name] = $query->count();
        }
        ksort($uniqueAttendanceByDepartment);

        $attendanceByDepartment = [];
        foreach ( $college->departments as $department ) {
            $attendance = new Attendance();
            $attendance->forDepartment($department);
            $query = $attendance->buildQuery();
            $attendanceByDepartment[$department->name] = $query->count();
        }
        ksort($attendanceByDepartment);

        $uniqueAttendanceByRank = [];
        foreach ( Rank::all() as $rank ) {
            $attendance = new Attendance();
            $attendance->forCollege($college);
            $attendance->forRank($rank);
            $attendance->unique(TRUE);
            $query = $attendance->buildQuery();
            $key = $rank->name;
            if ( $key == NULL ) {
                $key = "Rank is Undefined";
            }
            $uniqueAttendanceByRank[$key] = $query->count();
        }
        ksort($uniqueAttendanceByRank);

        $attendanceByRank = [];
        foreach ( Rank::all() as $rank ) {
            $attendance = new Attendance();
            $attendance->forCollege($college);
            $attendance->forRank($rank);
            $query = $attendance->buildQuery();
            $key = $rank->name;
            if ( $key == NULL ) {
                $key = "Rank is Undefined";
            }
            $attendanceByRank[$key] = $query->count();
        }
        ksort($attendanceByRank);

        return view('pdPSR::report.pd.college.show', compact(
            'college', 'terms',
            'uniqueAttendanceByDepartment',
            'attendanceByDepartment',
            'uniqueAttendanceByRank',
            'attendanceByRank'
        ));
    }

}
