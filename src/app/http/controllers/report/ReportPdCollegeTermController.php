<?php

namespace Yeltrik\PdPSR\app\http\controllers\report;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Yeltrik\PdPSR\app\report\Attendance;
use Yeltrik\UniMbr\app\models\Rank;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniTrm\app\models\Term;

class ReportPdCollegeTermController extends Controller
{

    /**
     * ReportPdCollegeController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @param College $college
     * @param Term $term
     * @return Application|Factory|View
     */
    public function show(College $college, Term $term)
    {
        $terms = Term::all();

        $uniqueAttendanceByDepartmentForTerm = [];
        foreach ( $college->departments as $department ) {
            $attendance = new Attendance();
            $attendance->forDepartment($department);
            $attendance->forTerm($term);
            $attendance->unique(TRUE);
            $query = $attendance->buildQuery();
            $uniqueAttendanceByDepartmentForTerm[$department->name] = $query->count();
        }
        ksort($uniqueAttendanceByDepartmentForTerm);

        $attendanceByDepartmentForTerm = [];
        foreach ( $college->departments as $department ) {
            $attendance = new Attendance();
            $attendance->forDepartment($department);
            $attendance->forTerm($term);
            $query = $attendance->buildQuery();
            $attendanceByDepartmentForTerm[$department->name] = $query->count();
        }
        ksort($attendanceByDepartmentForTerm);

        $uniqueAttendanceByRankForTerm = [];
        foreach ( Rank::all() as $rank ) {
            $attendance = new Attendance();
            $attendance->forCollege($college);
            $attendance->forRank($rank);
            $attendance->forTerm($term);
            $attendance->unique(TRUE);
            $query = $attendance->buildQuery();
            $key = $rank->name;
            if ( $key == NULL ) {
                $key = "Rank is Undefined";
            }
            $uniqueAttendanceByRankForTerm[$key] = $query->count();
        }
        ksort($uniqueAttendanceByRankForTerm);

        $attendanceByRankForTerm = [];
        foreach ( Rank::all() as $rank ) {
            $attendance = new Attendance();
            $attendance->forCollege($college);
            $attendance->forRank($rank);
            $attendance->forTerm($term);
            $query = $attendance->buildQuery();
            $key = $rank->name;
            if ( $key == NULL ) {
                $key = "Rank is Undefined";
            }
            $attendanceByRankForTerm[$key] = $query->count();
        }
        ksort($attendanceByRankForTerm);

        //dd([
        //    $uniqueAttendanceByDepartmentForTerm,
        //    $attendanceByDepartmentForTerm,
        //    $uniqueAttendanceByRankForTerm,
        //    $attendanceByRankForTerm
        //]);

        return view('pdPSR::report.pd.college.term.show', compact(
            'college', 'terms', 'term',
            'uniqueAttendanceByDepartmentForTerm',
            'attendanceByDepartmentForTerm',
            'uniqueAttendanceByRankForTerm',
            'attendanceByRankForTerm'
        ));
    }

}
