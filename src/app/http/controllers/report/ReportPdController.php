<?php

namespace Yeltrik\PdPSR\app\http\controllers\report;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ReportPdController extends Controller
{

    /**
     * ReportPdController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('pdPSR::report.pd.index');
    }

}
