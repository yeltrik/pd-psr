<?php

namespace Yeltrik\PdPSR\app\models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Yeltrik\PdPSR\database\factories\ProgramFactory;

/**
 * Class Program
 *
 * @property int id;
 * @property string title;
 *
 * @property Collection tags
 *
 * @package Yeltrik\PdPSR\App\Models
 */
class Program extends Model
{
    use HasFactory;

    protected $connection = 'pd_psr';
    public $table = 'programs';

    /**
     * @return ProgramFactory
     */
    public static function newFactory()
    {
        return new ProgramFactory();
    }

    /**
     * @return HasMany
     */
    public function tags()
    {
        return $this->hasMany(ProgramTag::class);
    }

}
