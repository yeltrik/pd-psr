<?php

namespace Yeltrik\PdPSR\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\PdPSR\database\factories\ProgramTagFactory;

/**
 * Class ProgramTag
 *
 * @property int id
 * @property int program_id
 * @property string tag_title
 *
 * @property Program program
 *
 * @package Yeltrik\PdPSR\App\Models
 */
class ProgramTag extends Model
{
    use HasFactory;

    protected $connection = 'pd_psr';
    public $table = 'program_tags';

    /**
     * @return ProgramTagFactory
     */
    public static function newFactory()
    {
        return new ProgramTagFactory();
    }

    /**
     * @return BelongsTo
     */
    public function program()
    {
        return $this->belongsTo(Program::class);
    }

}
