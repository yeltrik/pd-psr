<?php

namespace Yeltrik\PdPSR\app\models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Yeltrik\PdPSR\database\factories\RosterFactory;
use Yeltrik\Profile\app\models\Profile;

/**
 * Class Roster
 *
 * @property int id
 * @property int session_id
 * @property int user_id
// * @property int person_id
 * @property int profile_id
 * @property bool attended
 *
 * @property Profile profile
 * @property Session session
 * @property User user
// * @property Person person
 *
 * @package Yeltrik\PdPSR\App\Models
 */
class Roster extends Model
{
    use HasFactory;

    protected $connection = 'pd_psr';
    public $table = 'rosters';

    /**
     * @return RosterFactory
     */
    public static function newFactory()
    {
        return new RosterFactory();
    }

    /**
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    /**
     * @return BelongsTo
     */
    public function session()
    {
        return $this->belongsTo(Session::class);
    }

    /**
     * @return HasMany
     */
    public function tags()
    {
        return $this->hasMany(RosterTag::class);
    }

}
