<?php

namespace Yeltrik\PdPSR\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\PdPSR\database\factories\RosterTagFactory;

/**
 * Class RosterTag
 *
 * @property int id
 * @property int session_id
 * @property string tag_title
 *
 * @property Roster roster
 *
 * @package Yeltrik\PdPSR\App\Models
 */
class RosterTag extends Model
{
    use HasFactory;

    protected $connection = 'pd_psr';
    public $table = 'roster_tags';

    /**
     * @return RosterTagFactory
     */
    public static function newFactory()
    {
        return new RosterTagFactory();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function roster()
    {
        return $this->belongsTo(Roster::class);
    }

}
