<?php

namespace Yeltrik\PdPSR\app\models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Yeltrik\PdPSR\database\factories\SessionFactory;

/**
 * Class Session
 *
 * @property int id
 * @property int program_id
 * @property string title
 * @property string start
 * @property string end
 *
 * @property Program program
 * @property Collection tags
 *
 * @package Yeltrik\PdPSR\App\Models
 */
class Session extends Model
{
    use HasFactory;

    protected $connection = 'pd_psr';
    public $table = 'sessions';

    /**
     * @return SessionFactory
     */
    public static function newFactory()
    {
        return new SessionFactory();
    }

    /**
     * @return BelongsTo
     */
    public function program()
    {
        return $this->belongsTo(Program::class);
    }

    /**
     * @return HasMany
     */
    public function tags()
    {
        return $this->hasMany(SessionTag::class);
    }

}
