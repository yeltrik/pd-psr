<?php

namespace Yeltrik\PdPSR\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\PdPSR\database\factories\SessionTagFactory;

/**
 * Class SessionTag
 *
 * @property int id
 * @property int session_id
 * @property string tag_title
 *
 * @property Session session
 *
 * @package Yeltrik\PdPSR\App\Models
 */
class SessionTag extends Model
{
    use HasFactory;

    protected $connection = 'pd_psr';
    public $table = 'session_tags';

    /**
     * @return SessionTagFactory
     */
    public static function newFactory()
    {
        return new SessionTagFactory();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function session()
    {
        return $this->belongsTo(Session::class);
    }

}
