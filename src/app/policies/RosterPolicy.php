<?php

namespace Yeltrik\PdPSR\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Yeltrik\PdPSR\app\models\Roster;

class RosterPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny(User $user)
    {
        return TRUE;
    }

    public function view(User $user, Roster $roster)
    {
        return TRUE;
    }

}
