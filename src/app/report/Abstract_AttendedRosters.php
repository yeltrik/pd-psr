<?php


namespace Yeltrik\PdPSR\app\report;


use Illuminate\Database\Eloquent\Builder;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\ProfileUniMbr;
use Yeltrik\PdPSR\app\models\Roster;
use Yeltrik\UniMbr\app\models\Faculty;
use Yeltrik\UniMbr\app\models\Member;
use Yeltrik\UniOrg\app\models\Department;

/**
 * Class Abstract_AttendedRosters
 * @package Yeltrik\PdPSR\app\report
 */
abstract class Abstract_AttendedRosters
{

    /**
     * @param Department $department
     */
    protected function attendedFacultyForDepartment(Department $department)
    {
        return Faculty::query()
            ->where('department_id', '=', $department->id)
            ->whereIn('member_id',
                $this->attendedMembers()
                ->pluck('id')
            );
    }

    /**
     *
     */
    protected function attendedMembers()
    {
        return Member::query()
            ->whereIn('id',
                $this->attendedProfileUniMbrs()
                    ->pluck('member_id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    protected function attendedProfileUniMbrs()
    {
//        dd($this->attendedRosters()
//            ->pluck('profile_id')
//            ->toArray());
        return ProfileUniMbr::query()
            ->whereIn('profile_id',
                $this->attendedRosters()
                    ->pluck('profile_id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    protected function attendedRosters()
    {
        return $this->rosters()
            ->where('attended', '=', TRUE);
    }

    /**
     * @return Builder
     */
    protected function rosters()
    {
        return Roster::query();
    }

    abstract function toArray(): array;

}
