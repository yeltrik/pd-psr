<?php


namespace Yeltrik\PdPSR\app\report;


use Yeltrik\UniOrg\app\models\College;

/**
 * Class Abstract_ForCollege
 * @package Yeltrik\PdPSR\app\report
 */
abstract class Abstract_ForCollege extends Abstract_AttendedRosters
{

    private College $college;

    /**
     * UniqueAttendanceByDepartmentForCollege constructor.
     * @param College $college
     */
    public function __construct(College $college)
    {
        $this->college = $college;
    }

    /**
     * @return College
     */
    public function college()
    {
        return $this->college;
    }

}
