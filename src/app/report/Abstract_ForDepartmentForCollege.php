<?php


namespace Yeltrik\PdPSR\app\report;


use Illuminate\Database\Eloquent\Builder;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\ProfileUniMbr;
use Yeltrik\PdPSR\app\models\Roster;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\UniMbr\app\models\DepartmentHead;
use Yeltrik\UniMbr\app\models\Faculty;
use Yeltrik\UniMbr\app\models\Member;
use Yeltrik\UniMbr\app\models\Staff;
use Yeltrik\UniOrg\app\models\Department;

abstract class Abstract_ForDepartmentForCollege extends Abstract_ForCollege
{

    /**
     * @param Department $department
     * @return Builder
     */
    public function departmentHeadsForDepartment(Department $department)
    {
        return DepartmentHead::query()
            ->where('department_id', '=', $department->id);
    }

    /**
     * @param Department $department
     * @return Builder
     */
    public function facultyForDepartment(Department $department)
    {
        return Faculty::query()
            ->where('department_id', '=', $department->id);
    }

    public function membersForDepartment(Department $department)
    {
        return Member::query()
            ->orWhereIn('id',
                $this->departmentHeadsForDepartment($department)
                    ->pluck('id')
                    ->toArray()
            )
            ->orWhereIn('id',
                $this->facultyForDepartment($department)
                    ->pluck('id')
                    ->toArray()
            )
            ->orWhereIn('id',
                $this->staffForDepartment($department)
                    ->pluck('id')
                    ->toArray()
            );
    }

    /**
     * @param Department $department
     * @return Builder
     */
    public function profilesForDepartment(Department $department)
    {
        return Profile::query()
            ->whereIn('id',
                $this->rostersForDepartment($department)
                    ->pluck('profile_id')
                    ->toArray()
            );
    }

    /**
     * @param Department $department
     * @return Builder
     */
    public function profileUniMbrsForDepartment(Department $department)
    {
        return ProfileUniMbr::query()
            ->whereIn('member_id',
                $this->membersForDepartment($department)
                    ->pluck('id')->toArray()
            );
    }

    /**
     * @param Department $department
     * @return Builder
     */
    public function rostersForDepartment(Department $department)
    {
        return Roster::query()
            ->where('attended', '=', TRUE)
            ->whereIn('profile_id',
                $this->profileUniMbrsForDepartment($department)
                    ->pluck('profile_id')
                    ->toArray()
            );
    }

    /**
     * @param Department $department
     * @return Builder
     */
    public function staffForDepartment(Department $department)
    {
        return Staff::query()
            ->where('department_id', '=', $department->id);
    }

}
