<?php


namespace Yeltrik\PdPSR\app\report;


use Illuminate\Database\Eloquent\Builder;
use Yeltrik\ImportPDAsana\app\models\PDPSRUniTrm;
use Yeltrik\ImportProfileAsanaUniMbr\app\models\ProfileUniMbr;
use Yeltrik\PdPSR\app\models\Roster;
use Yeltrik\PdPSR\app\models\Session;
use Yeltrik\Profile\app\models\Profile;
use Yeltrik\UniMbr\app\models\Dean;
use Yeltrik\UniMbr\app\models\DepartmentHead;
use Yeltrik\UniMbr\app\models\Faculty;
use Yeltrik\UniMbr\app\models\Member;
use Yeltrik\UniMbr\app\models\Rank;
use Yeltrik\UniMbr\app\models\Staff;
use Yeltrik\UniMbr\app\models\Student;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\Department;
use Yeltrik\UniTrm\app\models\Term;

class Attendance
{

    const ATTENDED = TRUE;

    private ?College $college = NULL;
    private ?Department $department = NULL;
    private ?Term $term = NULL;
    private ?Rank $rank = NULL;
    private bool $unique = FALSE;

    /**
     * @return Builder
     */
    public function buildQuery()
    {
        $rostersQuery = $this->rosters(static::ATTENDED);

        if ($this->college instanceof College) {
            $rostersQuery->whereIn('profile_id',
                $this->profilesForCollege()
                    ->pluck('id')
                    ->toArray()
            );
        }

        if ($this->department instanceof Department) {
            $rostersQuery->whereIn('profile_id',
                $this->profilesForDepartment()
                    ->pluck('id')
                    ->toArray()
            );
        }

        if ($this->rank instanceof Rank) {
            $rostersQuery->whereIn('session_id',
                $this->profilesForRank()
                    ->pluck('id')
                    ->toArray()
            );
        }

        if ($this->term instanceof Term) {
            $rostersQuery->whereIn('session_id',
                $this->sessionsForTerm()
                    ->pluck('id')
                    ->toArray()
            );
        }

        if ($this->unique === TRUE) {
            $rostersQuery->distinct('profile_id');
        }

        return $rostersQuery;
//        dd([
//            $this->college,
//            $this->department,
//            $this->term,
//            $rostersQuery->count(),
//            $rostersQuery->get()
//        ]);
    }

    /**
     * @return Builder
     */
    private function deansForCollege()
    {
        return Dean::query()
            ->where('college_id', '=', $this->college->id);
    }

    /**
     * @return Builder
     */
    private function departmentsForCollege()
    {
        return Department::query()
            ->where('college_id', '=', $this->college->id);
    }

    /**
     * @return Builder
     */
    private function departmentHeadsForCollege()
    {
        return DepartmentHead::query()
            ->whereIn('department_id',
                $this->departmentsForCollege()
                    ->pluck('id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    private function departmentHeadsForDepartment()
    {
        return DepartmentHead::query()
            ->where('department_id', '=', $this->department->id);
    }

    /**
     * @return Builder
     */
    private function facultyForCollege()
    {
        return Faculty::query()
            ->whereIn('department_id',
                $this->departmentsForCollege()
                    ->pluck('id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    private function facultyForDepartment()
    {
        return Faculty::query()
            ->where('department_id', '=', $this->department->id);
    }

    /**
     * @return Builder
     */
    private function facultyForRank()
    {
        return Faculty::query()
            ->where('rank_id', '=', $this->rank->id);
    }

    /**
     * @param College $college
     */
    public function forCollege(College $college)
    {
        $this->college = $college;
    }

    /**
     * @param Department $department
     */
    public function forDepartment(Department $department)
    {
        $this->department = $department;
    }

    /**
     * @param Rank $rank
     */
    public function forRank(Rank $rank)
    {
        $this->rank = $rank;
    }

    /**
     * @param Term $term
     */
    public function forTerm(Term $term)
    {
        $this->term = $term;
    }

    /**
     * @return Builder
     */
    private function membersForCollege()
    {
        return Member::query()
            ->orWhereIn('id',
                $this->deansForCollege()
                    ->pluck('member_id')
                    ->toArray()
            )
            ->orWhereIn('id',
                $this->departmentHeadsForCollege()
                    ->pluck('member_id')
                    ->toArray()
            )
            ->orWhereIn('id',
                $this->facultyForCollege()
                    ->pluck('member_id')
                    ->toArray()
            )
            ->orWhereIn('id',
                $this->staffForCollege()
                    ->pluck('member_id')
                    ->toArray()
            );
            //->orWhereIn('id',
            //    $this->studentsForCollege()
            //        ->pluck('member_id')
            //        ->toArray()
            //);
    }

    /**
     * @return Builder
     */
    private function membersForDepartment()
    {
        return Member::query()
            ->orWhereIn('id',
                $this->departmentHeadsForDepartment()
                    ->pluck('member_id')
                    ->toArray()
            )
            ->orWhereIn('id',
                $this->facultyForDepartment()
                    ->pluck('member_id')
                    ->toArray()
            )
            ->orWhereIn('id',
                $this->staffForDepartment()
                    ->pluck('member_id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    private function membersForRank()
    {
        return Member::query()
            ->orWhereIn('id',
                $this->facultyForRank()
                    ->pluck('member_id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    private function pdPSRUniTrmForTerm()
    {
        return PDPSRUniTrm::query()
            ->where('term_id', '=', $this->term->id);
    }

    /**
     * @return Builder
     */
    private function profilesForCollege()
    {
        return Profile::query()
            ->whereIn('id',
                $this->profileUniMbrForCollege()
                    ->pluck('id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    private function profilesForDepartment()
    {
        return Profile::query()
            ->whereIn('id',
                $this->profileUniMbrForDepartment()
                    ->pluck('id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    private function profilesForRank()
    {
        return Profile::query()
            ->whereIn('id',
                $this->profileUniMbrForRank()
                    ->pluck('id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    private function profileUniMbrForCollege()
    {
        return ProfileUniMbr::query()
            ->whereIn('member_id',
                $this->membersForCollege()
                    ->pluck('id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    private function profileUniMbrForDepartment()
    {
        return ProfileUniMbr::query()
            ->whereIn('member_id',
                $this->membersForDepartment()
                    ->pluck('id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    private function profileUniMbrForRank()
    {
        return ProfileUniMbr::query()
            ->whereIn('member_id',
                $this->membersForRank()
                    ->pluck('id')
                    ->toArray()
            );
    }

    /**
     * @param bool|null $attended
     * @return Builder
     */
    private function rosters(bool $attended = NULL)
    {
        $rosterQuery = Roster::query();
        if ($attended !== NULL) {
            $rosterQuery->where('attended', '=', $attended);
        }
        return $rosterQuery;
    }

    /**
     * @return Builder
     */
    private function sessionsForTerm()
    {
        return Session::query()
            ->whereIn('id',
                $this->pdPSRUniTrmForTerm()
                    ->pluck('session_id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    private function staffForCollege()
    {
        return Staff::query()
            ->whereIn('department_id',
                $this->departmentsForCollege()
                    ->pluck('id')
                    ->toArray()
            );
    }

    /**
     * @return Builder
     */
    private function staffForDepartment()
    {
        return Staff::query()
            ->where('department_id', '=', $this->department->id);
    }

    /**
     * @return Builder
     */
    private function studentsForCollege()
    {
        // Students don't have a College, the have a university
        // Students for University
        return Student::query()
            ->where('college_id', '=', $this->college->id);
    }

    /**
     * @param bool $unique
     */
    public function unique(bool $unique)
    {
        $this->unique = $unique;
    }

}
