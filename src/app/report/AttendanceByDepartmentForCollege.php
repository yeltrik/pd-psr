<?php


namespace Yeltrik\PdPSR\app\report;


class AttendanceByDepartmentForCollege extends Abstract_ForDepartmentForCollege
{

    /**
     * @return array
     */
    public function toArray() : array
    {
        $array = [];
        foreach ($this->college()->departments as $department) {
            $array[$department->name] = $this->rostersForDepartment($department)->count();
        }
        return $array;
    }

}
