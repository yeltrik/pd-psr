<?php


namespace Yeltrik\PdPSR\app\report;



/**
 * Class UniqueAttendanceByDepartmentForCollege
 * @package Yeltrik\PdPSR\app\report
 */
class UniqueAttendanceByDepartmentForCollege extends Abstract_ForDepartmentForCollege
{

    /**
     * @return array
     */
    public function toArray() : array
    {
        $array = [];
        foreach ($this->college()->departments as $department) {
            $array[$department->name] = $this->profilesForDepartment($department)->count();
        }
        return $array;
    }

}
