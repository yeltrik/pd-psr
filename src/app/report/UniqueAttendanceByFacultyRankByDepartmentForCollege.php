<?php


namespace Yeltrik\PdPSR\app\report;


class UniqueAttendanceByFacultyRankByDepartmentForCollege extends Abstract_ForDepartmentForCollege
{

    CONST UNKNOWN_RANK_TEXT = "Rank Unavailable";
    
    /**
     * @return array
     */
    function toArray(): array
    {
        // (Reverse)
        // Poster where Attended
        // Profile for Roster
        // Member for Profile
        // Faculty for Member (For Department)
        // Rank for Faculty

        $array = [];
        foreach ($this->college()->departments as $department) {
            $rosters = $this->attendedRosters();
            //dd($rosters->get());
            $profileUniMbrs = $this->attendedProfileUniMbrs();
            //dd($profileUniMbrs->get());
            $members = $this->attendedMembers();
            //dd($members->get());
            $faculty = $this->attendedFacultyForDepartment($department);
            //dd($faculty->get());
            // Get Rank For Faculty
            foreach ( $faculty->get() as $faculty ) {
                //dd($faculty);
                $key = $faculty->rank->name;
                if ( $key == NULL ) {
                    $key = static::UNKNOWN_RANK_TEXT;
                }
                if ( !array_key_exists($key, $array)) {
                    $array[$key] = 0;
                }
                $array[$key]++;
            }
        }
        return $array;
    }

}
