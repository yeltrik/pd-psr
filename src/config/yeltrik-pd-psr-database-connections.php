<?php

use Illuminate\Support\Str;

return

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    [
        // Connection
        'pd_psr' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('PD_PSR_DB_DATABASE', database_path('pd_psr.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        // SQLITE
        'pd_psr_sqlite_example' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('PD_PSR_DB_DATABASE', database_path('pd_psr.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        // MYSQL
        'pd_psr_mysql_example' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('PD_PSR_DB_HOST', '127.0.0.1'),
            'port' => env('PD_PSR_DB_PORT', '3306'),
            'database' => env('PD_PSR_DB_DATABASE', 'pd_psr'),
            'username' => env('PD_PSR_DB_USERNAME', 'pd_psr'),
            'password' => env('PD_PSR_DB_PASSWORD', ''),
            'unix_socket' => env('PD_PSR_DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

    ];
