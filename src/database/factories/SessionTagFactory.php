<?php

namespace Yeltrik\PdPSR\database\factories;

use Yeltrik\PdPSR\app\models\SessionTag;
use Illuminate\Database\Eloquent\Factories\Factory;

class SessionTagFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SessionTag::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tag_title' => $this->faker->word
        ];
    }
}
