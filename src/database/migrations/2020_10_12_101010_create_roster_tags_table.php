<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRosterTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pd_psr')->create('roster_tags', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('roster_id');
            $table->string('tag_title', 64);
            $table->timestamps();

            $table->unique(['roster_id', 'tag_title']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pd_psr')->dropIfExists('roster_tags');
    }
}
