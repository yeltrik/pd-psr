<?php

namespace Yeltrik\PdPSR\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\PdPSR\app\models\Program;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $programs = Program::factory()
            ->count(3)
            ->create();
    }
}
