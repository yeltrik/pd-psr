<?php

namespace Yeltrik\PdPSR\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\PdPSR\app\models\Program;
use Yeltrik\PdPSR\app\models\ProgramTag;

class ProgramTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tag = ProgramTag::factory()
            ->count(3)
            ->for(Program::factory())
            ->create();
    }
}
