<?php

namespace Yeltrik\PdPSR\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\PdPSR\app\models\Roster;
use Yeltrik\PdPSR\app\models\Session;
use Yeltrik\Profile\app\models\Profile;

class RosterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rosters = Roster::factory()
            ->count(3)
            ->for(
                Session::factory()
            )
            ->for(
                Profile::factory()
            )
            ->create();
    }
}
