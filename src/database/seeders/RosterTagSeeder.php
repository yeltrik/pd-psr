<?php

namespace Yeltrik\PdPSR\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\PdPSR\app\models\Roster;
use Yeltrik\PdPSR\app\models\RosterTag;
use Yeltrik\PdPSR\app\models\Session;
use Yeltrik\Profile\app\models\Profile;

class RosterTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rosterTags = RosterTag::factory()
            ->count(3)
            ->for(
                Roster::factory()
                    ->for(
                        Session::factory()
                    )->for(
                        Profile::factory()
                    )
            )
            ->create();
    }
}
