<?php

namespace Yeltrik\PdPSR\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\PdPSR\app\models\Program;
use Yeltrik\PdPSR\app\models\Session;

class SessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sessions = Session::factory()
            ->count(3)
            ->for(Program::factory())
            ->create();
    }
}
