<?php

namespace Yeltrik\PdPSR\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\PdPSR\app\models\Program;
use Yeltrik\PdPSR\app\models\Session;
use Yeltrik\PdPSR\app\models\SessionTag;

class SessionTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sessionTags = SessionTag::factory()
            ->count(3)
            ->for(Session::factory())
            ->create();
    }
}
