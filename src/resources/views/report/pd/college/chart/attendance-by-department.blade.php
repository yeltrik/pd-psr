<canvas id="myChart3" ></canvas>
<script>
    $( document ).ready(function() {
        var ctx = document.getElementById('myChart3').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: {!!json_encode(array_keys($attendanceByDepartment))!!},
                datasets: [{
                    data: {!!json_encode(array_values($attendanceByDepartment))!!},
                    backgroundColor: {!! \Yeltrik\Color\app\Colors::fromArrayByKeys($attendanceByDepartment)->jsonEncodedRGBAColors() !!}
                }]
            },
            options: {
                aspectRatio: 1.5,
                title: {
                    display: true,
                    text: 'Attendance By Department'
                },
                legend: {
                    display: true,
                    position: 'right'
                }
            }
        });
    });
</script>
