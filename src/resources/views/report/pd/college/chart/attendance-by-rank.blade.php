<canvas id="myChart4" ></canvas>
<script>
    $( document ).ready(function() {
        var ctx = document.getElementById('myChart4').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: {!!json_encode(array_keys($attendanceByRank))!!},
                datasets: [{
                    data: {!!json_encode(array_values($attendanceByRank))!!},
                    backgroundColor: {!! \Yeltrik\Color\app\Colors::fromArrayByKeys($attendanceByRank)->jsonEncodedRGBAColors() !!}
                }]
            },
            options: {
                aspectRatio: 1.5,
                title: {
                    display: true,
                    text: 'Attendance By Rank'
                },
                legend: {
                    display: true,
                    position: 'right'
                }
            }
        });
    });
</script>
