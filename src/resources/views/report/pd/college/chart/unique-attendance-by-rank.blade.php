<canvas id="myChart2" ></canvas>
<script>
    $( document ).ready(function() {
        var ctx = document.getElementById('myChart2').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: {!!json_encode(array_keys($uniqueAttendanceByRank))!!},
                datasets: [{
                    data: {!!json_encode(array_values($uniqueAttendanceByRank))!!},
                    backgroundColor: {!! \Yeltrik\Color\app\Colors::fromArrayByKeys($uniqueAttendanceByRank)->jsonEncodedRGBAColors() !!}
                }]
            },
            options: {
                aspectRatio: 1.5,
                title: {
                    display: true,
                    text: 'Unique Attendance By Rank'
                },
                legend: {
                    display: true,
                    position: 'right'
                }
            }
        });
    });
</script>
