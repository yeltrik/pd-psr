<ul class="list-group">
    <li class="list-group-item active">Attendance by Rank</li>
    @include('pdPSR::report.pd.college.comment.li.all-pd-sessions')
    @include('pdPSR::report.pd.college.comment.li.faculty')
    @include('pdPSR::report.pd.college.comment.li.attended')
    @include('pdPSR::report.pd.college.comment.li.college')
    @include('pdPSR::report.pd.college.comment.li.group-by-rank')
</ul>
