<ul class="list-group">
    <li class="list-group-item active">Unique Attendance by Department</li>
    @include('pdPSR::report.pd.college.comment.li.all-pd-sessions')
    @include('pdPSR::report.pd.college.comment.li.faculty-staff-student')
    @include('pdPSR::report.pd.college.comment.li.attended')
    @include('pdPSR::report.pd.college.comment.li.college')
    @include('pdPSR::report.pd.college.comment.li.unique')
    @include('pdPSR::report.pd.college.comment.li.group-by-department')
</ul>
