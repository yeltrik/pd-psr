<div class="row">
    <div class="col-sm-6">
        <h4>
            Unique Attendance By Department
        </h4>
        @include('pdPSR::report.pd.college.nav-tab.tab-list.unique-attendance-by-department')
    </div>
    <div class="col-sm-6">
        <h4>
            Unique Attendance By Rank
        </h4>
        @include('pdPSR::report.pd.college.nav-tab.tab-list.unique-attendance-by-rank')
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <h4>
            Attendance By Department
        </h4>
        @include('pdPSR::report.pd.college.nav-tab.tab-list.attendance-by-department')
    </div>
    <div class="col-sm-6">
        <h4>
            Attendance By Rank
        </h4>
        @include('pdPSR::report.pd.college.nav-tab.tab-list.attendance-by-rank')
    </div>
</div>
