@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active">
                Colleges
            </a>
            @foreach($colleges as $college)
                <a href="{{ route('reports.pds.colleges.show', [$college]) }}" class="list-group-item list-group-item-action">
                    {{ $college->name }}
                </a>
            @endforeach
        </div>

    </div>
@endsection
