@extends('layouts.app')

@section('content')
    <div class="container">

        <h3>{{$college->name}}</h3>

        @include('pdPSR::report.pd.college.overview')

        @includeIf('report::blurbs.request-additional-data')

    </div>
@endsection
