<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Department</th>
        <th scope="col" class="text-center">Unique Attendance</th>
    </tr>
    </thead>
    <tbody>
    @foreach($uniqueAttendanceByDepartment as $departmentName => $count)
        <tr>
            <th scope="row">{{$departmentName}}</th>
            <td class="text-center">{{$count}}</td>
        </tr>
    @endforeach

    </tbody>
</table>
