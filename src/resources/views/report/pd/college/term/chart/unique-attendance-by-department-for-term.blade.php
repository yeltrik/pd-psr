<canvas id="myChart1" ></canvas>
<script>
    $( document ).ready(function() {
        var ctx = document.getElementById('myChart1').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: {!!json_encode(array_keys($uniqueAttendanceByDepartmentForTerm))!!},
                datasets: [{
                    data: {!!json_encode(array_values($uniqueAttendanceByDepartmentForTerm))!!},
                    backgroundColor: {!! \Yeltrik\Color\app\Colors::fromArrayByKeys($uniqueAttendanceByDepartmentForTerm)->jsonEncodedRGBAColors() !!}

                }]
            },
            options: {
                aspectRatio: 1.5,
                title: {
                    display: true,
                    text: 'Unique Attendance By Department'
                },
                legend: {
                    display: true,
                    position: 'right'
                }
            }
        });
    });
</script>
