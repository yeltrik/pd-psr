<ul class="list-group">
    <li class="list-group-item active">Unique Attendance by Department For Term</li>
    @include('pdPSR::report.pd.college.comment.li.term')
    @include('pdPSR::report.pd.college.comment.li.faculty-staff-student')
    @include('pdPSR::report.pd.college.comment.li.attended')
    @include('pdPSR::report.pd.college.comment.li.college')
    @include('pdPSR::report.pd.college.comment.li.unique')
    @include('pdPSR::report.pd.college.comment.li.group-by-department')
</ul>
