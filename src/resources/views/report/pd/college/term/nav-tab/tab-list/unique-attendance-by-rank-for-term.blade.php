<?php $navTabId="nav-tab3"; ?>

<?php $navChartTabId="nav-chart-tab3"; ?>
<?php $navChartTabContentId="nav-chart-tab-content3"; ?>
<?php $navTableTabId="nav-table-tab3"; ?>
<?php $navTableTabContentId="nav-table-tab-content3"; ?>
<?php $navListTabId="nav-list-tab3"; ?>
<?php $navListTabContentId="nav-list-tab-content3"; ?>
<?php $navCommentTabId="nav-comment-tab3"; ?>
<?php $navCommentTabContentId="nav-comment-tab-content3"; ?>

<nav>
    <div class="nav nav-tabs" id="{{$navTabId}}" role="tablist">
        <a class="nav-item nav-link active" id="{{$navChartTabId}}" data-toggle="tab" href="#{{$navChartTabContentId}}" role="tab"
           aria-controls="{{$navChartTabContentId}}" aria-selected="true">
            <i class="fas fa-chart-pie"></i> Chart
        </a>
        <a class="nav-item nav-link" id="{{$navTableTabId}}" data-toggle="tab" href="#{{$navTableTabContentId}}" role="tab"
           aria-controls="{{$navTableTabContentId}}" aria-selected="false">
            <i class="fas fa-table"></i> Table
        </a>
{{--        <a class="nav-item nav-link" id="{{$navListTabId}}" data-toggle="tab" href="#{{$navListTabContentId}}" role="tab"--}}
{{--           aria-controls="{{$navListTabContentId}}" aria-selected="false">--}}
{{--            <i class="fas fa-list"></i> List--}}
{{--        </a>--}}
        <a class="nav-item nav-link" id="{{$navCommentTabId}}" data-toggle="tab" href="#{{$navCommentTabContentId}}" role="tab"
           aria-controls="{{$navCommentTabContentId}}" aria-selected="false">
            <i class="far fa-comment-alt"></i> Comment
        </a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane show active" id="{{$navChartTabContentId}}" role="tabpanel" aria-labelledby="{{$navChartTabId}}">
        @if(empty(array_filter($uniqueAttendanceByRankForTerm)))
            @include('pdPSR::report.pd.college.chart.no-data')
        @else
            @include('pdPSR::report.pd.college.term.chart.unique-attendance-by-rank-for-term')
            @include('pdPSR::report.pd.college.data-count', ['count' => array_sum($uniqueAttendanceByRankForTerm), 'title' => "unique participants"])
        @endif
    </div>
    <div class="tab-pane fade" id="{{$navTableTabContentId}}" role="tabpanel" aria-labelledby="{{$navTableTabId}}">
        @include('pdPSR::report.pd.college.term.table.unique-attendance-by-rank-for-term')
    </div>
{{--    <div class="tab-pane fade" id="{{$navListTabContentId}}" role="tabpanel" aria-labelledby="{{$navListTabId}}">--}}

{{--    </div>--}}
    <div class="tab-pane fade" id="{{$navCommentTabContentId}}" role="tabpanel" aria-labelledby="{{$navCommentTabId}}">
        @include('pdPSR::report.pd.college.term.comment.unique-attendance-by-rank')
    </div>
</div>
