<div class="card w-100" style="width: 18rem;">
    <div class="card-header">
        Overview

        <div class="float-right">
            @include('pdPSR::report.pd.college.uni-trm.select')
        </div>
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item">
            @include('pdPSR::report.pd.college.term.grid.all-college')
            <div class="float-right">
                @include('pdPSR::report.pd.college.btn.export-college')
            </div>
        </li>
    </ul>
</div>

