<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Department</th>
        <th scope="col" class="text-center">Attendance</th>
    </tr>
    </thead>
    <tbody>
    @foreach($attendanceByDepartmentForTerm as $departmentName => $count)
        <tr>
            <th scope="row">{{$departmentName}}</th>
            <td class="text-center">{{$count}}</td>
        </tr>
    @endforeach

    </tbody>
</table>
