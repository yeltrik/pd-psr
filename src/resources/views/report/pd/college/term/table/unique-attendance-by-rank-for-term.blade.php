<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Rank</th>
        <th scope="col" class="text-center">Unique Attendance</th>
    </tr>
    </thead>
    <tbody>
    @foreach($uniqueAttendanceByRankForTerm as $rankName => $count)
        <tr>
            <th scope="row">{{$rankName}}</th>
            <td class="text-center">{{$count}}</td>
        </tr>
    @endforeach

    </tbody>
</table>
