<label for="report-pd-college-uni-trm-select">
    Select Term
</label>
<select
    id="report-pd-college-uni-trm-select"
    onchange="selectTerm(this);"
>
    <option value="all">
        All
    </option>
    @foreach($terms as $term1)
        <option
            value="{{$term1->id}}"
            @if(isset($term) && $term->id === $term1->id) selected @endif
        >
            {{$term1->fullAbbreviation}}
        </option>
    @endforeach
</select>

<script>
    function selectTerm(element) {
        let val = $(element).val();

        let route = "{{ route('reports.pds.colleges.show', $college) }}";

        if (val) {
            if (val == "all") {
                window.location = route;
            } else {
                window.location = route+"/term/" + val;
            }
        }
        return false;
    }
</script>
