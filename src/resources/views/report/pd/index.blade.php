@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active">
                Reporting Views
            </a>
            <a href="{{ route('reports.pds.colleges.index') }}" class="list-group-item list-group-item-action">By College</a>
        </div>

    </div>
@endsection
