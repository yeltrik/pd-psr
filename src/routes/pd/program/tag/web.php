<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\PdPSR\app\http\controllers\ProgramTagController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('pd/program/tag',
    [ProgramTagController::class, 'index'])
    ->name('pds.programs.tags.index');

Route::get('pd/program/tag/{tag}',
    [ProgramTagController::class, 'show'])
    ->name('pds.programs.tags.show');
