<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\PdPSR\app\http\controllers\ProgramController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require 'tag/web.php';

Route::get('pd/program/',
    [ProgramController::class, 'index'])
    ->name('pds.programs.index');

Route::get('pd/program/{program}',
    [ProgramController::class, 'show'])
    ->name('pds.programs.show');

