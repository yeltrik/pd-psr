<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\PdPSR\app\http\controllers\RosterTagController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('pd/roster/tag',
    [RosterTagController::class, 'index'])
    ->name('pds.rosters.tags.index');

Route::get('pd/roster/tag/{tag}',
    [RosterTagController::class, 'show'])
    ->name('pds.rosters.tags.show');
