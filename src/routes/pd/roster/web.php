<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\PdPSR\app\http\controllers\RosterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require 'tag/web.php';

Route::get('pd/roster/',
    [RosterController::class, 'index'])
    ->name('pds.rosters.index');

Route::get('pd/roster/{roster}',
    [RosterController::class, 'show'])
    ->name('pds.rosters.show');
