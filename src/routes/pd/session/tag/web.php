<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\PdPSR\app\http\controllers\SessionTagController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('pd/session/tag',
    [SessionTagController::class, 'index'])
    ->name('pds.sessions.tags.index');

Route::get('pd/session/tag/{tag}',
    [SessionTagController::class, 'show'])
    ->name('pds.sessions.tags.show');
