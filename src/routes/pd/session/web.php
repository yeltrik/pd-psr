<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\PdPSR\app\http\controllers\SessionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require 'tag/web.php';

Route::get('pd/session/',
    [SessionController::class, 'index'])
    ->name('pds.sessions.index');

Route::get('pd/session/{session}',
    [SessionController::class, 'show'])
    ->name('pds.sessions.show');
