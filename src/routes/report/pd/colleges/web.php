<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\PdPSR\app\http\controllers\report\ReportPdCollegeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require 'term/web.php';

Route::get('report/pd/college',
    [ReportPdCollegeController::class, 'index'])
    ->name('reports.pds.colleges.index');

Route::get('report/pd/college/{college}',
    [ReportPdCollegeController::class, 'show'])
    ->name('reports.pds.colleges.show');

