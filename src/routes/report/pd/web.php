<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\PdPSR\app\http\controllers\report\ReportPdController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('report/pd',
    [ReportPdController::class, 'index'])
    ->name('reports.pds.index');

require 'colleges/web.php';
