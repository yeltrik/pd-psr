<?php

namespace Yeltrik\PdPSR\tests\feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Yeltrik\PdPSR\app\models\RosterTag;

class RosterTagTest extends TestCase
{

    public function testRouteIndex()
    {
        $user = $this->getUser();
        $response = $this->actingAs($user, 'web')
            ->get(route('pds.rosters.tags.index', []));
        $response->assertStatus(200);
    }

    public function testRouteShow()
    {
        $user = $this->getUser();
        $rosterTag = RosterTag::query()->inRandomOrder()->firstOrFail();
        $response = $this->actingAs($user, 'web')
            ->get(route('pds.rosters.tags.show', [$rosterTag]));
        $response->assertStatus(200);
    }

    public function getUser()
    {
        return User::query()->inRandomOrder()->firstOrFail();
    }

}
