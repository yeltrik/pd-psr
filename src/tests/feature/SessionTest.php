<?php

namespace Yeltrik\PdPSR\tests\feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Yeltrik\PdPSR\app\models\Session;

class SessionTest extends TestCase
{

    public function testRouteIndex()
    {
        $user = $this->getUser();
        $response = $this->actingAs($user, 'web')
            ->get(route('pds.sessions.index', []));
        $response->assertStatus(200);
    }

    public function testRouteShow()
    {
        $user = $this->getUser();
        $session = Session::query()->inRandomOrder()->firstOrFail();
        $response = $this->actingAs($user, 'web')
            ->get(route('pds.sessions.show', [$session]));
        $response->assertStatus(200);
    }

    public function getUser()
    {
        return User::query()->inRandomOrder()->firstOrFail();
    }

}
